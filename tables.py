from random import randint, shuffle
from time import time

'''
------------------------- Raffinage R0 ---------------------------------

R0: Faire réviser des tables de multiplication au user

-------------------- Exemple de déroulement du programme ---------------

	Ex: La machine demande au user "Quelle table voulez-vous réviser ?"
	Le user répond "7"
	La machine lui affiche "Combien font 7 x 3?"
	Le user répond "21"
	La machine lui affiche "Correct."
	La machine propose "7 x 2?"
	Le user répond "12"	
	La machine lui affiche "Erreur."
	La machine propose à nouveau "Combien font 7 x 3?"
	Le user répond "21"
	La machine lui affiche "Correct."
	La machine lui affiche "Combien font 7 x 6?"
	Le user répond "42"
	La machine lui affiche "Correct."
	La machine propose "7 x 8?"
	Le user répond "56"
	La machine lui affiche "Correct."
	La machine propose "Combien font 7 x 1?"
	Le user répond "7"
	La machine lui affiche "Correct."
	La machine propose à nouveau "Combien font 7 x 6?"
	Le user répond "41"
	La machine lui affiche "Erreur."
	La machine lui affiche "Combien font 7 x 4?"
	Le user répond "28"
	La machine lui affiche "Correct."
	La machine propose "7 x 8?"
	Le user répond "56"
	La machine lui affiche "Correct."
	La machine propose à nouveau "Combien font 7 x 1?"
	Le user répond "7"
	La machine lui affiche "Correct."
	La machine lui affiche 'Vous avez 8 bonnes réponses, c'est bien!"

------------------------- Raffinage R1 ---------------------------------

R1 : Comment faire réviser des tables de multiplication au user?
		- Demander au user la table qu'il souhaite réviser
		- Poser 10 questions au user sur la table du multiplication			
		- Afficher son résultat au user

------------------------- Raffinage R2 ---------------------------------

R2 : Comment "Poser 10 questions au user sur la table du multiplication"?
		- Tant que les 10 questions ne sont pas posées
			- Poser une question au user
			- Afficher une observation


------------------------- Raffinage R3 ---------------------------------

R3 : "Demander au user la table qu'il souhaite réviser"?
		- Poser la question au user
		- Stocker sa réponse dans la variable "num_table"


R3 : "Poser 10 questions au user sur la table du multiplication"
		- Initialiser la réponse du user à 0 dans la variable result
		- Initialiser le score du user à 0 dans la variable score
		- Tant que les 10 questions ne sont pas posées
			- Stocker un chiffre aleatoire entre 1 et 10 dans la variable "x"
			- Stocker le résultat attendu de la multiplication dans la variable "result"
			- Stocker la réponse du user à la question sur la mutliplication dans "answer"
			- Afficher une observation selon la réponse donnée
				- Si answer == result
					- Afficher "Correct."
					- Incrémenter le score du user
				- Sinon
					- Afficher "Erreur."

R3 : "Afficher son résultat au user"
		- Afficher au sein de la même phrase
			- Le score fait par le user 
			- Le message lié à son score

'''

####################################################################################################################################



def table():
	'''Faire réviser des tables de multiplication au user'''

	# Demander au user la table qu'il souhaite réviser
	num_table: int = int(input("Quelle table de multiplication souhaitez-vous réviser?\nRéponse: "))	# table à réviser

	result: int = 0 # réponse du user à la question posée
	score: int = 0 # score final du user

	# Tant que les 10 questions ne sont pas posées
	for _ in range(10):
		x: int = randint(1,10) # chiffre aleatoire entre 1 et 10 
		result: int = x * num_table # résultat de la multiplication
		answer: int = int(input(f'> Combien font {num_table} x {x} ? \nRéponse: ')) # réponse du user
		# Afficher une observation selon la réponse donnée
		if answer == result:
			print("Correct.")
			# Incrémenter le score du joueur
			score += 1
		else:
			print("Erreur.")
	
	# Afficher son résultat au user
	message: int = " " # Initialisation du message
	if score == 10:
		message = "Excellent !"
	elif score == 9:
		message = "Très bien."
	elif score >= 7:
		message = "Bien."
	elif score >= 4:
		message = "Moyen."
	elif score == 0:
		message = "Est-ce que tu l’as fait exprès ?"
	else:
		message = "Il faut retravailler cette table."

	print(f'** Votre score est de {score}. {message} **')	




####################################################################################################################################

'''1.3. Compléter le programme pour afficher la question pour laquelle l’utilisateur a mis le plus de temps pour répondre ainsi la moyenne des temps de réponse. 
		On utilisera la fonction time du module time pour récupérer l’heure actuelle.'''


def table_time():
	'''Faire réviser des tables de multiplication au user et afficher la question où il a mis le plus de temps à répondre.'''

	# Demander au user la table qu'il souhaite réviser
	num_table: int = int(input("Quelle table de multiplication souhaitez-vous réviser?\nRéponse: "))	# table à réviser

	result: int = 0 # réponse du user à la question posée
	score: int = 0 # score final du user

	# Initialiser les variables pour calculer le temps 
	y: int = 0 # variable pour entrer dans la comparaison du temps de réponse
	one_time: float = 0 # temps de réponse à 1 question
	average_time: int = 0 # temps moyen de réponse aux 10 questions
	tricky: int = 0 # chiffre où le user a mis le plus de temps à répondre

	# Tant que les 10 questions ne sont pas posées
	for _ in range(10):
		x: int = randint(1,10) # chiffre aleatoire entre 1 et 10 
		result: int = x * num_table # résultat de la multiplication

		# Mesurer le temps de réponse à chaque question
		start = time() # On lance le chronomètre
		answer: int = int(input(f'> Combien font {num_table} x {x} ? \nRéponse: ')) # réponse du user
		end = time() # On arrête le chronomètre
		one_time = round((end - start),2) # On calcule le temps qu'a mis le user pour répondre
		average_time += one_time # On incrémente le temps de réponse total

		# Calculer la réponse donnée la moins rapide
		if one_time > y:
			y = one_time
			tricky = x 

		# Afficher une observation selon la réponse donnée
		if answer == result:
			print("Correct.")
			# Incrémenter le score du joueur
			score += 1
		else:
			print("Erreur.")
	# Calculer le temps moyen qu'a mis le user pour répondre aux questions(sans compter le temps passé sur la question la plus difficile)
	average_time = round(((average_time-y)/9), 2) 
	# Afficher au user la question la plus difficile pour lui et sa moyenne
	print(f'Vous avez mis plus de temps à répondre à la question "Combien font {num_table} * {tricky} ?", vous avez mis {y} secondes alors que votre moyenne était de {average_time}.')

	# Afficher son résultat au user
	message: int = " " # Initialisation du message
	if score == 10:
		message = "Excellent !"
	elif score == 9:
		message = "Très bien."
	elif score >= 7:
		message = "Bien."
	elif score >= 4:
		message = "Moyen."
	elif score == 0:
		message = "Est-ce que tu l’as fait exprès ?"
	else:
		message = "Il faut retravailler cette table."

	print(f'** Votre score est de {score}. {message} **')	


####################################################################################################################################


'''1.4. Ajouter la possibilité pour l’utilisateur de continuer à réviser ses tables de multiplications. 
		On lui posera la question « On continue les révisions (o/n) ? ». 
		Le programme s’arrêtera si l’utilisateur répond « n » ou « N ». 
		Dans le cas, contraire il fait réviser une table choisie par l’utilisateur.'''

def table_continue():
	'''Faire réviser des tables de multiplication au user, afficher la question où il a mis le plus de temps à répondre et lui proposer de rejouer.'''
	
	play_again = True # Variable instanciée pour être sûr d'entrer dans la boucle de jeu
	# Lancer le jeu tant que le joueur répond "o" ou "O"
	while play_again:
		# Demander au user la table qu'il souhaite réviser
		num_table: int = int(input("Quelle table de multiplication souhaitez-vous réviser?\nRéponse: "))	# table à réviser

		result: int = 0 # réponse du user à la question posée
		score: int = 0 # score final du user

		# Initialiser les variables pour calculer le temps 
		y: int = 0 # variable pour entrer dans la comparaison du temps de réponse
		one_time: float = 0 # temps de réponse à 1 question
		average_time: int = 0 # temps moyen de réponse aux 10 questions
		tricky: int = 0 # chiffre où le user a mis le plus de temps à répondre



		# Tant que les 10 questions ne sont pas posées
		for _ in range(10):
			x: int = randint(1,10) # chiffre aleatoire entre 1 et 10 
			result: int = x * num_table # résultat de la multiplication

			# Mesurer le temps de réponse à chaque question
			start = time() # On lance le chronomètre
			answer: int = int(input(f'> Combien font {num_table} x {x} ? \nRéponse: ')) # réponse du user
			end = time() # On arrête le chronomètre
			one_time = round((end - start),2) # On calcule le temps qu'a mis le user pour répondre
			average_time += one_time # On incrémente le temps de réponse total

			# Calculer la réponse donnée la moins rapide
			if one_time > y:
				y = one_time
				tricky = x 

			# Afficher une observation selon la réponse donnée
			if answer == result:
				print("Correct.")
				# Incrémenter le score du joueur
				score += 1
			else:
				print("Erreur.")
		# Calculer le temps moyen qu'a mis le user pour répondre aux questions(sans compter le temps passé sur la question la plus difficile)
		average_time = round(((average_time-y)/9), 2) 
		# Afficher au user la question la plus difficile pour lui et sa moyenne
		print(f'Vous avez mis plus de temps à répondre à la question "Combien font {num_table} * {tricky} ?", vous avez mis {y} secondes alors que votre moyenne était de {average_time}.')

		# Afficher son résultat au user
		message: int = " " # Initialisation du message
		if score == 10:
			message = "Excellent !"
		elif score == 9:
			message = "Très bien."
		elif score >= 7:
			message = "Bien."
		elif score >= 4:
			message = "Moyen."
		elif score == 0:
			message = "Est-ce que tu l’as fait exprès ?"
		else:
			message = "Il faut retravailler cette table."

		print(f'** Votre score est de {score}. {message} **')	

		# Proposer au jouer de rejouer
		rejouer: str = str(input("On continue les révisions (o/n) ?")).lower()
		
		if rejouer == 'n':
			play_again = False # Instancie la variable à False pour stopper le jeu

	print("A bientôt!")	



####################################################################################################################################


'''1.5. Modifier le programme pour qu’il ne pose pas deux fois de suite la même multiplication.'''

def table_once():
	'''Faire réviser des tables de multiplication au user sans poser 2 fois la même question,
	   afficher la question où il a mis le plus de temps à répondre et lui proposer de rejouer.'''
	
	play_again = True # Variable instanciée pour être sûr d'entrer dans la boucle de jeu
	# Lancer le jeu tant que le joueur répond "o" ou "O"
	while play_again:
		proposal: bool = False
		while not proposal:
			try: 
				# Demander au user la table qu'il souhaite réviser
				num_table: int = int(input("Quelle table de multiplication souhaitez-vous réviser?\nRéponse: "))	# table à réviser
				proposal = True
			except ValueError:
				print("Merci d'entrer un nombre.")

		result: int = 0 # réponse du user à la question posée
		score: int = 0 # score final du user

		# Initialiser les variables pour calculer le temps 
		y: int = 0 # variable pour entrer dans la comparaison du temps de réponse
		one_time: float = 0 # temps de réponse à 1 question
		average_time: int = 0 # temps moyen de réponse aux 10 questions
		tricky: int = 0 # chiffre où le user a mis le plus de temps à répondre


		# créer une liste de 10 chiffres
		questions: list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] 
		shuffle(questions) # La liste devient aléatoire

		# Tant que les 10 questions ne sont pas posées
		while questions:
			x: int = questions.pop() # chiffre aleatoire et non répété entre 1 et 10 
			result: int = x * num_table # résultat de la multiplication

			# Mesurer le temps de réponse à chaque question
			start = time() # On lance le chronomètre
			proposal: bool = False
			while not proposal:
				try:
					answer: int = int(input(f'> Combien font {num_table} x {x} ? \nRéponse: ')) # réponse du user
					proposal = True
				except ValueError:
					print("Merci d'entrer un nombre.")
			end = time() # On arrête le chronomètre
			one_time = round((end - start),2) # On calcule le temps qu'a mis le user pour répondre
			average_time += one_time # On incrémente le temps de réponse total

			# Calculer la réponse donnée la moins rapide
			if one_time > y:
				y = one_time
				tricky = x 

			# Afficher une observation selon la réponse donnée
			if answer == result:
				print("Correct.")
				# Incrémenter le score du joueur
				score += 1
			else:
				print("Erreur.")
		# Calculer le temps moyen qu'a mis le user pour répondre aux questions(sans compter le temps passé sur la question la plus difficile)
		average_time = round(((average_time-y)/9), 2) 
		# Afficher au user la question la plus difficile pour lui et sa moyenne
		print(f'Vous avez mis plus de temps à répondre à la question "Combien font {num_table} * {tricky} ?", vous avez mis {y} secondes alors que votre moyenne était de {average_time}.')

		# Afficher son résultat au user
		message: int = " " # Initialisation du message
		if score == 10:
			message = "Excellent !"
		elif score == 9:
			message = "Très bien."
		elif score >= 7:
			message = "Bien."
		elif score >= 4:
			message = "Moyen."
		elif score == 0:
			message = "Est-ce que tu l’as fait exprès ?"
		else:
			message = "Il faut retravailler cette table."

		print(f'** Votre score est de {score}. {message} **')	


		again: bool = True
		while again:
			# Proposer au jouer de rejouer
			try:
				rejouer: str = str(input("On continue les révisions (o/n) ?")).lower()
			except ValueError:
				print("Merci d'entrer une lettre. ")
				continue
			# Stopper le jeu quand réponse user = 'n'
			if rejouer == 'n':
				play_again = False # Instancie la variable à False pour stopper le jeu
				again = False # Instancie la variable à False pour sortir de la boucle
			# Rappeler le joueur qu'il n'a qu'un choix entre deux réponses
			elif rejouer!= 'n' and rejouer !='o':
				print("Merci d'entrer 'o' ou 'O' pour rejouer, 'n' ou 'N' pour quitter ")
				continue
			# Rejouer quand réponse user = 'o'
			else:
				break
			
	print("A bientôt!")	

table_once()