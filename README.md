# tables_multi

Raffinages

Objectifs
— Écrire des programmes plus ambitieux
— Mettre ne pratique la méthode des raffinage
— Aborder les exceptions

Exercice 1 : Réviser les tables de multiplication

On souhaite aider à la révision des tables de multiplication. Le principe est de disposer d’un
programme qui pose 10 multiplications pour une table donnée (le nombre de gauche est celui
de la table, celui de droite est choisi aléatoirement entre 1 et 10). Le programme demande à
l’utilisateur le résultat et lui indique s’il a bien répondu (« Correct » ou « Erreur »). À la fin, il
lui affiche le nombre de bonnes réponses ainsi qu’un message. 

Le message est :
— « Excellent !» si toutes les réponses sont justes,
— « Très bien. » s’il n’a commis qu’une seule erreur,
— « Bien. » s’il n’a pas fait plus de trois erreurs,
— « Moyen. » s’il a 4, 5 ou 6 bonnes réponses,
— « Il faut retravailler cette table. » s’il a 3 bonnes réponses ou moins et
— « Est-ce que tu l’as fait exprès ?" si toutes les réponses sont fausses.

La table à réviser sera demandée en début de programme (pas de contrôle).

1.1. Pour obtenir un nombre aléatoire, on utilisera la fonction randint du module random. Faire
help('random.randint') sous l’interpréteur Python pour avoir sa documentation.

1.2. Écrire le programme demandé.

1.3. Compléter le programme pour afficher la question pour laquelle l’utilisateur a mis le plus de
temps pour répondre ainsi la moyenne des temps de réponse. On utilisera la fonction time du
module time pour récupérer l’heure actuelle.

1.4. Ajouter la possibilité pour l’utilisateur de continuer à réviser ses tables de multiplications. On
lui posera la question « On continue les révisions (o/n) ? ». Le programme s’arrêtera si l’utilisateur répond « n » ou « N ». Dans le cas, contraire il fait réviser une table choisie par l’utilisateur.

1.5. Modifier le programme pour qu’il ne pose pas deux fois de suite la même multiplication.